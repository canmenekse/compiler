# About
A compiler that translates the input language to output language, see Rules.txt for the rules of those languages


# How to Run:
Makefile creates two executables

* pl : This program gets the command line arguments and calls ./detailedPL<inputfilename>outputname

* detailedPL: This is the actual program that parses translates etc.

After using the makefile you can just run it with

./pl inputfilename outputname

I tried it with:

Flex version  : 2.5.35
bison version 2.5
gcc version : 4.6.3
OS: ubuntu-12.04.5
