%{
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "Node.h"
#include  <assert.h>
#define LEN (8* sizeof(int) - 1) / 3 + 2
//I did not implemented the Vector I used an implementation I found online
int variableCount = 0;
int labelCount=0;



void yyerror(const char *s)
{
	printf("%s\n", s);
}

char * concatenateTwoCharPtr(char *charPtr1, char * charPtr2);
void updateValue (char * id , int value);
char* generateFreshTempVariable();
char * generateFreshLabel();
Node * createNode(int value,char * var,char *cde);
char * generateIF(char * ExprVar, char *elseCde,char * ifStmtCde);
char * generateCustomIF(char *begin, char * after , char * ExprVar, char * ifStmtCde,char *label);

%}
%union {
int line;
char *name;
int value;
Node *nodePtr;
}
%token <value> tINTNUM <name>tID tBEGIN tEND tIF tTHEN tELSE tWHILE tFALSE tTRUE tCOMMA tLPAR tRPAR tSLPAR tSRPAR tASSIGN
%type <nodePtr> Expr
%type <nodePtr> Op
%type <nodePtr> AsgnStmt
%type <nodePtr> Prgrm
%type <nodePtr> StmtBlk
%type <nodePtr> StmtLst
%type <nodePtr> Stmt
%type <nodePtr> IfStmt
%type <nodePtr> WhlStmt
%left tNOT
%left tMULT
%left tPLUS
%left tSMALLER
%left tEQ
%left tAND
%% 
/*Grammar follows */
Prgrm: StmtBlk 
{
	$$=createNode(-1,$1->cde,$1->cde);
	printf("\n%s ",$1->cde);
}
;
StmtBlk: tBEGIN StmtLst tEND
{
 char *concatenated = "\n";
 concatenated = concatenateTwoCharPtr(concatenated,$2->cde);
 $$=createNode(-1,concatenated,concatenated);
}
;
StmtLst: 
Stmt StmtLst 
{
	char * concatenated = concatenateTwoCharPtr($1->cde,"\n");
	concatenated=concatenateTwoCharPtr(concatenated,$2->cde);
	$$=createNode(-1,concatenated,concatenated); 
}
		 |Stmt {$$=createNode(-1,$1->cde,$1->cde);}
		 ;
Stmt :  AsgnStmt  {$$=createNode(-1,$1->cde,$1->cde); }
	   |IfStmt {$$=createNode(-1,$1->cde,$1->cde); }
	   |WhlStmt
;
AsgnStmt:tID tASSIGN Expr tCOMMA
{
	
	
	char * concatenated = concatenateTwoCharPtr($3->cde,$1);
	concatenated = concatenateTwoCharPtr(concatenated,"=");
	
	concatenated=concatenateTwoCharPtr(concatenated,$3->var);
	concatenated=concatenateTwoCharPtr(concatenated,";");
	$$=createNode(-1,concatenated,concatenated);
	
}
;
IfStmt: tIF tLPAR Expr tRPAR tTHEN StmtBlk tELSE StmtBlk
{ 
	char * concatenated = $3->cde;
	char * ifprt = generateIF($3->var,$8->cde,$6->cde);
	concatenated = concatenateTwoCharPtr(concatenated,ifprt);
	$$=createNode(-1,concatenated,concatenated);
	
}
;
WhlStmt: tWHILE tLPAR Expr tRPAR StmtBlk
{
	char * beginLabel = generateFreshLabel();
	char * label=generateFreshLabel();
	char * afterLabel = generateFreshLabel();
	char * concatenated = "";
	concatenated = concatenateTwoCharPtr(concatenated,beginLabel);
	concatenated = concatenateTwoCharPtr(concatenated, ": ");
	concatenated = concatenateTwoCharPtr(concatenated,$3->cde);
	char * ifStatement = generateCustomIF(beginLabel,afterLabel,$3->var,$5->cde,label);
	concatenated = concatenateTwoCharPtr(concatenated,ifStatement);
	$$=createNode(-1,concatenated,concatenated);	
	
}
;
Expr: 
tINTNUM {$$=createNode(-1,$1,"");} | 
tFALSE {$$=createNode(-1,"false","");} | 
tTRUE {$$=createNode(-1,"true","");} | 
tID 
{
	$$ = createNode (-1,$1,"");
}
| tLPAR Expr tRPAR 
{
  $$=createNode(-1,$2->var,$2->cde);
}
| 
Expr Op Expr
{
	char *newVar =strdup(generateFreshTempVariable());
	
	
	char * concatenated="";
	concatenated = concatenateTwoCharPtr(concatenated,$1->cde);
	
	
	concatenated = concatenateTwoCharPtr(concatenated,$3->cde);
	concatenated = concatenateTwoCharPtr(concatenated,newVar);
	concatenated = concatenateTwoCharPtr(concatenated," = ");
	concatenated = concatenateTwoCharPtr(concatenated,$1->var);
	concatenated = concatenateTwoCharPtr(concatenated," ");
	concatenated = concatenateTwoCharPtr(concatenated,$2->cde);
	concatenated = concatenateTwoCharPtr(concatenated," ");
	concatenated = concatenateTwoCharPtr(concatenated,$3->var);
	concatenated = concatenateTwoCharPtr(concatenated,"\n");
	
	
	$$ = createNode(-1,newVar ,concatenated);
	
}
;
Op: 
	tPLUS {$$=createNode(-1," ","+");}	
	| tMULT {$$=createNode(-1," ","*");}
	| tSMALLER {$$=createNode(-1," ","<");}
	| tEQ {$$=createNode(-1," ","==");}
	| tAND {$$=createNode(-1," ","and");}
;
%%		 
int main()
{	
	printf("\n////STARTED TRANSLATING//////\n");
	if(yyparse())
	{
		printf("ERROR\n" );
		//return 1;
	}
	else
	{
		printf("\nGRAMMAR IS OK\n");
	}
	printf("\n///FINISHED TRANSLATING//\n");
	return 0;
}


char * generateCustomIF(char *begin, char * after , char * ExprVar, char * ifStmtCde,char *label)
{
	char * concatenated = "IF (";
	char * elseCde = "";
	
	concatenated = concatenateTwoCharPtr(concatenated,ExprVar);
	concatenated =concatenateTwoCharPtr(concatenated,") GOTO ");
	concatenated =concatenateTwoCharPtr(concatenated,label);
	
	concatenated = concatenateTwoCharPtr(concatenated,elseCde);
	
	concatenated = concatenateTwoCharPtr(concatenated,"\n");
	concatenated = concatenateTwoCharPtr(concatenated, "GOTO ");
	concatenated = concatenateTwoCharPtr(concatenated,after);
	concatenated = concatenateTwoCharPtr(concatenated,"\n");
	concatenated = concatenateTwoCharPtr(concatenated,label);
	concatenated = concatenateTwoCharPtr(concatenated,":");
	concatenated = concatenateTwoCharPtr(concatenated,ifStmtCde);
	concatenated = concatenateTwoCharPtr(concatenated,"\n");
	concatenated = concatenateTwoCharPtr(concatenated, "GOTO ");
	concatenated = concatenateTwoCharPtr(concatenated, begin);
	concatenated = concatenateTwoCharPtr(concatenated, "\n");
	concatenated = concatenateTwoCharPtr(concatenated,after);
	concatenated = concatenateTwoCharPtr(concatenated, ":");
	return concatenated;	

}




char * generateIF(char * ExprVar, char *elseCde,char * ifStmtCde)
{
	char * concatenated = "IF (";
	char * label = generateFreshLabel();
	concatenated = concatenateTwoCharPtr(concatenated,ExprVar);
	concatenated =concatenateTwoCharPtr(concatenated,") GOTO ");
	concatenated =concatenateTwoCharPtr(concatenated,label);
	concatenated = concatenateTwoCharPtr(concatenated,elseCde);
	char * label2 = generateFreshLabel();
	concatenated = concatenateTwoCharPtr(concatenated,"\n");
	concatenated = concatenateTwoCharPtr(concatenated, "GOTO ");
	concatenated = concatenateTwoCharPtr(concatenated,label2);
	concatenated = concatenateTwoCharPtr(concatenated,"\n");
	concatenated = concatenateTwoCharPtr(concatenated,label);
	concatenated = concatenateTwoCharPtr(concatenated,":");
	concatenated = concatenateTwoCharPtr(concatenated,ifStmtCde);
	concatenated = concatenateTwoCharPtr(concatenated,"\n");
	concatenated = concatenateTwoCharPtr(concatenated, label2);
	concatenated = concatenateTwoCharPtr(concatenated, ":");
	return concatenated;
}

char * generateFreshTempVariable()
{
	
	char str[LEN];
	sprintf(str, "%d", variableCount);

	char * ret = strdup(str);
	variableCount++;
	ret= concatenateTwoCharPtr("t_",ret);
	return ret;
}


char * generateFreshLabel()
{
	
	char str[LEN];
	sprintf(str, "%d", labelCount);

	char * ret = strdup(str);
	labelCount++;
	ret= concatenateTwoCharPtr("L_",ret);
	return ret;
}

char * getPreviousFreshTempVariable()
{
	
	char str[LEN];
	sprintf(str, "%d", variableCount-1);

	char * ret = strdup(str);
	variableCount++;
	ret= concatenateTwoCharPtr("t_",ret);
	return ret;
}





char * concatenateTwoCharPtr(char *charPtr1, char * charPtr2)
{
	char * concatenated;
	int charPtr1Length=strlen(charPtr1);
	int charPtr2Length=strlen(charPtr2);		
	concatenated=malloc(charPtr1Length+charPtr2Length+2);		
	strcpy(concatenated,charPtr1);
	strcat(concatenated,charPtr2);
	return concatenated;	
}

Node * createNode(int value,char * var,char *cde)
{
	Node * ret = (Node *) malloc (sizeof(Node));
	ret->value = value;
	ret->var = var;
	ret->cde=cde;
	return (ret);
}








